import glob
import understanding_mat_uncompleted as umu

matfiles = [f for f in glob.glob("*.mat")]


for m in matfiles:		
	try:	
		umu.parseCSV(m)
	except IndexError as e:
		print "No valid file - empty data"
		break

