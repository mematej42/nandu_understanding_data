import glob
import understanding_mat_uncompleted as umu
import pandas as pd


matfiles = [f for f in glob.glob("*.mat")]

writedf = pd.DataFrame()

for m in matfiles:		
	try:	
		result = umu.parseCSV(m) 
		if result.shape[1] > 2:		
			print result.shape[1]			
			writedf = pd.concat([writedf, result], ignore_index=True, sort=False)
	except IndexError as e:
		print "No valid file - empty data"
		break


filename = "Result"+'_out.csv'
writedf.to_csv(filename, index = False)

