#Playing with data - matej / july 13, 2018

# reading measurments from patient 
# input marker

import numpy as np
import pandas as pd


def parseMatFallPrevetionBPHR(interested_keys, mat):
	result = pd.DataFrame()	
		
	try:	
		# Parsing all signals of mesaurments
		marked_signal = mat[interested_keys].dtype.names[11]
		temp = parseMatFallPrevetionPatient(interested_keys, 11, mat)
		temp1 = parseMatFallPrevetionPatient(interested_keys, 3, mat)
	
		data = np.concatenate(temp[0])	
		data1 = np.concatenate(temp1[0])	
		markers = np.concatenate(temp[1])
	
		table_start = markers.tolist().index(1)

		# define where you are wanting a signal
		mBP = data[table_start-120:table_start+60]
		HR = data1[table_start-120:table_start+60]

		write = pd.DataFrame(mBP).T
		write['Signal'] = "mBP"	
		write['Collapse'] = 1

		write1 = pd.DataFrame(HR).T
		write1['Signal'] = "HR"	
		write1['Collapse'] = 1

		result = pd.concat([write, write1], ignore_index=True, sort=False)
	except ValueError as e:
		print "No valid file............................................."
	return result



def parseMatFallPrevetionPatient(argument, j, mat):
	i=0		
	temp = []	
	markers = []
	names_variable  = mat[argument].dtype.names
	
	while True:
		try:	
			#print argument			
			#print "Dimenzija: "+ str(mat[argument][names_variable[j]][0,][0,][i,][0,][0,].shape)
			data = mat[argument][names_variable[j]][0,][0,][i,][0,][0,]			
			mark = np.empty(len(data)); mark.fill(i)
			temp.append(data)	
			markers.append(mark)
			i = i+1
		except IndexError as e:
			#print test	
			#print test.shape			
			#print(e.args) # (10, 'list index out of range'
			break
	return [temp, markers]


# reading signals from one mesaurment and puting to csv file by markers in columns 
# input ['BPV', 'OscBP', 'HRV', 'IV', 'BRS_BRS2', 'BRS_BRS1', 'BRS_BRS0', 'CardiacParams', 'BPVsBP', 'BeatToBeat']
def parseMatFallPrevetionMeasurment(measurment_type, mat, name):
	result = pd.DataFrame()	
		
	# Parsing all signals of mesaurments
	j = 0
	while True:
		try:
			marked_signal = mat[measurment_type].dtype.names[j] 
			
			temp = parseMatFallPrevetionPatient(measurment_type, j, mat)

			data = np.concatenate(temp[0])
			markers = np.concatenate(temp[1])
			col1 = measurment_type+'_'+marked_signal			
			col2 = "Mark_"+measurment_type+'_'+marked_signal
			d = {col1: data, col2: markers}
			writedf = pd.DataFrame(d)	
			if writedf.shape[0] > 3600:
				print "Parsing mesaurment's signals: " + measurment_type +"_"+ str(marked_signal)
				print "NOW WE ARE TALKING"
				#print writedf				
				result = pd.concat([result,writedf],axis=1)
				print result	
			j = j+1
		except IndexError as e:
			#print(e.args) # (10, 'list index out of range'
			break
	
	name = name.split('.')[0]
	filename = name+'_'+measurment_type+'_out.csv'
	result.to_csv(filename, index = False)
	return 0


def parseCSV(name):
	#MAIN PROGRAM
	# Import one Mat file - obly for understanding -  should automatize for files, when we decided how and what
	import scipy.io

	mat = scipy.io.loadmat(name)

	# dict-measurments: ['BPV', 'OscBP', 'HRV', 'IV', 'BRS_BRS2', 'BRS_BRS1', 'BRS_BRS0', 'CardiacParams', 'BPVsBP', 'BeatToBeat']
	keys = mat.keys()
	interested_keys = 'BeatToBeat'

	return parseMatFallPrevetionBPHR(interested_keys, mat)
	

